# -------------------------------------------------------------------------------------
# R Script to check specified simulation loop models for MSE (for Snapper MSE Project).
# by Athol Whitten, athol.whitten@mezo.com.au, 2017/2018
# -------------------------------------------------------------------------------------

# Set working directory to snap-mse directory to start.
setwd('C:/Bitbucket/Fisheries/snap-mse')

# Specify simulation model of interest:
sim_date     <- '20181121'
sim_scenario <- 'A'
sim_number   <- 1
sim_year     <- 2040
sim_model    <- 'sna-em'

# Create directory path for model of interest:
sim_dir  <- file.path("sims", sim_date, sim_scenario, sim_number, sim_year, sim_model)

# Set working directory to model of interest:
setwd(file.path(getwd(), sim_dir))

# Get SS outputs into R format, then produce and save plots to file:
sim_output <- r4ss::SS_output(dir = getwd(), covar = FALSE)
r4ss::SS_plots(replist = sim_output, uncertainty = FALSE)

# -----------------------------------------------------------------------------