# -----------------------------------------------------------------------------
# R Script for Generating Recruitment Scenarios used in Snapper MSE Project.
# Recruitment scenarios are based on the estimated recruitment 
# history from a recent SS model for the stock.
# Athol Whitten, athol.whitten@mezo.com.au
# Created February 2016, Updated October 2018
# -----------------------------------------------------------------------------

# Load required libraries:
library(r4ss)
library(ss3sim)
library(forecast)

# Set working directory to snap-mse directory to start.
setwd('C:/Bitbucket/Fisheries/snap-mse')

# Set SS model folder:
model_folder <- "models/sna-am"
output_folder <- "recdevs"
if(!dir.exists(output_folder)) dir.create(output_folder)

# Get SS outputs for current model:
replist <- SS_output(dir=model_folder, forecast=FALSE)

# Load current recruitment estimates (log-recdevs) from SS outputs:
parameters <- replist$parameters
recruit    <- replist$recruit
startyr    <- replist$startyr
endyr      <- replist$endyr
sigma_R_in <- replist$sigma_R_in

main_pars      <- parameters[substring(parameters$Label, 1, 12) %in% c("Main_RecrDev"), ]
main_recdev    <- main_pars$Value
main_years     <- as.numeric(substring(main_pars$Label, 14))

# Modfiy low outlier rec_devs to make time-series compatible with symetrical ARIMA modelling process:
max_recdev <- max(main_recdev)
main_recdev[which(main_recdev < -max_recdev)] <- -max_recdev

# Create a time-series object for the main recr. deviatons:
ts_main_recdev    <- ts(main_recdev, start=min(main_years), end=max(main_years), frequency=1)

# Plot the time-series object:
plot(ts_main_recdev)

# Fit an ARIMA(2,0,1) model to the time-series data:
fit_ts_recdev <- arima(ts_main_recdev, order=c(p=2, d=0, q=0))

# View and the fit results:
fit_ts_recdev

# Predict and plot the next 30 observations:
plot(forecast(fit_ts_recdev, 30))

# Create objects for blocks of years, with low and high recruitment:
low_years <- 1980:1994
high_years <- 1995:2016

# Get the main recruitment deviations for the high recent recdev period:
main_recdev_high <- main_recdev[which(main_years %in% high_years)]

# Create a time-series object for the main recr. deviatons (high recdev period):
ts_main_recdev_high <- ts(main_recdev_high, start=min(high_years), end=max(high_years), frequency=1)

# Fit an ARIMA model to the recent recruitment history (high recdev period, order = 2,0,0):
fit_ts_recdev_high <- arima(ts_main_recdev_high, order=c(p=2, d=0, q=0))

# View the fit results:
fit_ts_recdev_high

# Get the main recruitment deviations for the low early recdev period:
main_recdev_low <- main_recdev[which(main_years %in% low_years)]

# Create a time-series object for the main recr. deviatons (high recdev period):
ts_main_recdev_low <- ts(main_recdev_low, start=min(low_years), end=max(low_years), frequency=1)

# Fit an ARIMA model to the recent recruitment history (high recdev period, order = 2,0,0):
fit_ts_recdev_low <- arima(ts_main_recdev_low, order=c(p=2, d=0, q=0))

# View the fit results:
fit_ts_recdev_low

# Simulate recruitment deviations with ARIMA models fit above
# ---------------------------------------------------------------

# Set number of sims and forecast years required:
nrsims <- 500
nyrs  <- 24

# Scenario A
# -----------
# Future recruitment resembles recruitment from the recent high period:
a_ar1 <- coef(fit_ts_recdev_high)[1]
a_ar2 <- coef(fit_ts_recdev_high)[2]
a_var <- fit_ts_recdev_high$sigma2
a_mean <- mean(main_recdev_high)

# Simulate an ARIMA(2,0,0) sequence based on the parameters estimated above:
rts_sim_a <- arima.sim(list(order = c(2,0,0), ar = c(a_ar1, a_ar2)), mean = a_mean, sd = sqrt(a_var), n = nyrs)
plot(rts_sim_a)

# Create a matrix of 'nsim' random simulations:
rts_sim_df_a <- matrix(NA, nrow=nyrs, ncol=nrsims)
for(i in 1:nrsims){
	rts_sim_df_a[,i] <- arima.sim(list(order = c(2,0,0), ar = c(a_ar1, a_ar2)), mean = a_mean, sd = sqrt(a_var), n = nyrs)
}

# Coerce matrix created above to a time series:
rts_sim_df_a <- ts(rts_sim_df_a, start=max(main_years)+1, frequency=1)

# Write file with simulated recdev values:
write.csv(rts_sim_df_a, file=paste0(output_folder, "/Recr_Scn_A_new.csv"), row.names = FALSE)

# Plot the first four random simulations:
plot(rts_sim_df_a[, 1:4], main="Scenario A", type="o", col="blue", nc=2)
plot(rts_sim_df_a[, 5], main="Scenario A", ylab="Ln Recruitment Deviation", plot.type="single", col="blue", type="o", ylim=c(-3.5,4))
abline(h=0, col="grey")


# Scenario B
# -----------
# Future recruitment resembles recruitment from the entire main recdev period:
b_ar1 <- coef(fit_ts_recdev)[1]
b_ar2 <- coef(fit_ts_recdev)[2]
b_var <- fit_ts_recdev$sigma2
b_mean <- mean(main_recdev)

# Simulate an ARIMA(2,0,0) sequence based on the parameters estimated above:
rts_sim_b <- arima.sim(list(order = c(2,0,0), ar = c(b_ar1, b_ar2)), mean = b_mean, sd = sqrt(b_var), n = nyrs)
plot(rts_sim_b)

# Create a matrix of 'nsim' random simulations:
rts_sim_df_b <- matrix(NA, nrow=nyrs, ncol=nrsims)
for(i in 1:nrsims){
	rts_sim_df_b[,i] <- arima.sim(list(order = c(2,0,0), ar = c(b_ar1, b_ar2)), mean = b_mean, sd = sqrt(b_var), n = nyrs)
}

# Coerce matrix created above to a time series:
rts_sim_df_b <- ts(rts_sim_df_b, start=max(main_years)+1, frequency=1)

# Write file with simulated recdev values:
write.csv(rts_sim_df_b, file=paste0(output_folder, "/Recr_Scn_B_new.csv"), row.names = FALSE)

# Plot the first four random simulations:
plot(rts_sim_df_b[, 1:4], main="Scenario B", type="o", col="dark green", nc=2)
plot(rts_sim_df_b[, 1], main="Scenario B", ylab="Ln Recruitment Deviation", plot.type="single", col="dark green", type="o", ylim=c(-3.5,4))
abline(h=0, col="grey")

# Scenario C
# -----------
# Future recruitment resembles recruitment from the historical low period:
c_ar1 <- coef(fit_ts_recdev_low)[1]
c_ar2 <- coef(fit_ts_recdev_low)[2]
c_var <- fit_ts_recdev_low$sigma2
c_mean <- mean(main_recdev_low)

# Simulate an ARIMA(2,0,0) sequence based on the parameters estimated above:
rts_sim_c <- arima.sim(list(order = c(2,0,0), ar = c(c_ar1, c_ar2)), mean = c_mean, sd = sqrt(c_var), n = nyrs)
plot(rts_sim_c)

# Create a matrix of 'nsim' random simulations:
rts_sim_df_c <- matrix(NA, nrow=nyrs, ncol=nrsims)
for(i in 1:nrsims){
	rts_sim_df_c[,i] <- arima.sim(list(order = c(2,0,0), ar = c(c_ar1, c_ar2)), mean = c_mean, sd = sqrt(c_var), n = nyrs)
}

# Coerce matrix created above to a time series:
rts_sim_df_c <- ts(rts_sim_df_c, start=max(main_years)+1, frequency=1)

# Write file with simulated recdev values:
write.csv(rts_sim_df_c, file=paste0(output_folder, "/Recr_Scn_C_new.csv"), row.names = FALSE)

# Plot the first four random simulations:
plot(rts_sim_df_c[, 1:4], main="Scenario C", type="o", col="purple", nc=2)
plot(rts_sim_df_c[, 4], main="Scenario C", ylab="Ln Recruitment Deviation", plot.type="single", col="purple", type="o", ylim=c(-3.5,4))
abline(h=0, col="grey")

# --------------
# End of file.