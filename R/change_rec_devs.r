#' Replace recruitment deviations
#'
#' This function replaces the recruitment deviations in the
#' \code{ss3.par} file with those specified in \code{recdevs_new}, as
#' well as a comment (for debugging). It then writes a new file with
#' name \code{par_file_out} into the working directory.
#'
#' @param recdevs_new A vector of new recruitment deviations.
#' @template par_file_in
#' @template par_file_out
#' @return A modified SS3 \code{.par} file.
#' @author Cole Monnahan, Athol Whitten
#' @details This function does not need to be specified in a case file if you
#'   are running an ss3sim simulation through case files with
#'   \code{\link{run_ss3sim}}.
#' @export
#'
#' @examples
#' # Create a temporary folder for the output:
#' temp_path <- file.path(tempdir(), "ss3sim-recdev-example")
#' dir.create(temp_path, showWarnings = FALSE)
#'
#' par_file <- system.file("extdata", "models", "cod-om", "ss3.par",
#'   package = "ss3sim")
#' change_rec_devs(recdevs_new = rlnorm(100), par_file_in = par_file,
#'   par_file_out = paste0(temp_path, "/test.par"))

change_rec_devs <- function(recdevs_new, par_file_in = "ss3.par",
  par_file_out = "ss3.par", replace_all = TRUE){
  
  ## Pattern on line before vector of current recdevs:
  pattern <- "# recdev1"

  ## Stop and warn if par_file not found, else read and get pattern line number:
  if(!file.exists(par_file_in)) stop(paste("File", par_file_in,"not found"))
  par <- readLines(par_file_in, warn = FALSE)
  which.line <- grep(pattern = pattern, x = par) + 1

  ## Get old recdevs, remove leading space/s to apply length() operation:
  recdevs_old <- par[which.line]
  recdevs_old <- gsub("^\\s+|\\s+$", "", recdevs_old) # remove leading blank
  recdevs_old <- gsub("\\s+", " ", recdevs_old)       # remove >1 blanks
  recdevs_old <- as.numeric(unlist(strsplit(recdevs_old, split = " ")))

  ## Replace recdevs, all or partial, at end of series:
  if(replace_all == TRUE){
    ##  Make length(recdevs_new) equal to length(recdevs_old):
    recdevs_new <- recdevs_new[1:length(recdevs_old)]
  } else {
    ## Replace n recdevs, at end of series:
    recdevs_new <- replace(recdevs_old, 
      (length(recdevs_old) - length(recdevs_new) + 1):length(recdevs_old),
      recdevs_new)
  }

  ## Check if length of new and old recdevs match:
  if(length(recdevs_new) != length(recdevs_old)){
    stop("The new recdev vector isn't the same length as what is
      currently in the ss3.par file")
  }

  ## Replace w/ new recdevs, adding back a leading space:
  par[which.line] <- paste0(" ", recdevs_new, collapse="")
  
  ## Write new lines to file:
  writeLines(par, con = par_file_out)
}

