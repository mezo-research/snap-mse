# -----------------------------------------------------------------------------
# R Script to check performance of base EM vs OM for Snapper MSE.
# by Athol Whitten, athol.whitten@mezo.com.au, 2017/2018
# -----------------------------------------------------------------------------

# Load required libraries:
library(r4ss)
library(dplyr)
library(ggplot2)
library(tibble)
library(ss3sim)

#----------------------
# CONTROL SECTION
# ---------------------

# Set working directory to snap-mse directory to start:
setwd('C:/Bitbucket/Fisheries/snap-mse')

# Load mse helper functions, and override ss3sim library with modified ss3sim functions:
source("R/functions.R")
source_dir("R/ss3mse/")

# Get location of ss3 binary:
ss3_bin <- get_bin()

# Set locations and names for base operating and estimation models for testing:
models     <- "models"
am_base    <- "sna-am"
om_base    <- "sna-om"
em_base    <- "sna-em"
em_ctl	   <- "snaEM.ctl"
em_dat     <- "snaEM.dat"
om_ctl     <- "snaOM.ctl"
om_dat     <- "snaOM.dat"
om_dat_new <- "data.ss_new"

# Set sampling specs for estimation procedure:
age_samp_n 		   <- 200
age_samp_freq	   <- 1
len_samp_n 		   <- 200
len_samp_freq	   <- 1
surv_samp_err 	 <- 0.05
surv_samp_freq   <- 1

# Set number of simulations for EM diagnostic test:
n_sims <- 50

# Set model base years:
# Set frequency to run simulated assessments (years):
base_start_year     <- 1978
base_end_year       <- 2016
forecast_period     <- 20
base_years          <- base_start_year : base_end_year
base_forecast_years <- (base_end_year + 1) : (base_end_year + forecast_period)
base_all_years      <- base_start_year : (base_end_year + forecast_period)
nbase_years         <- length(base_years)
nall_year           <- length(base_all_years)

# Save diagnostic plot of OM vs EM simulations?
plot_save <- TRUE

#----------------------
# PROCEDURE SECTION
# ---------------------

# Create file path objects for starting base models:
am_base_dir <- file.path(models, am_base)
om_base_dir <- file.path(models, om_base)
em_base_dir <- file.path(models, em_base)

# Create new directory for base model test results (if required):
if(!dir.exists("test")) dir.create("test")

# Create directory for date of model tests:
test_date <- format(Sys.Date(), "%Y%m%d")
test_date_dir <- file.path("test", test_date)
if(!dir.exists(test_date_dir)) dir.create(test_date_dir)

# Copy base AM and OM files into test directory:
file.copy(am_base_dir, test_date_dir, recursive=TRUE)
file.copy(om_base_dir, test_date_dir, recursive=TRUE)

# Create empty directory for EM files:
if(!dir.exists(file.path(test_date_dir, em_base))) dir.create(file.path(test_date_dir, em_base))

# Get path for test operating and estimation models:
am_test_dir <- file.path(test_date_dir, am_base)
om_test_dir <- file.path(test_date_dir, om_base)
em_test_dir <- file.path(test_date_dir, em_base)

# Run base assessment model (AM):
setwd(file.path(getwd(), am_test_dir))
system(paste(ss3_bin))

# Get SS outputs for AM into R format, then produce and save plots to file:
test_am_output <- r4ss::SS_output(dir = getwd(), covar = TRUE)
SS_plots(replist = test_am_output, uncertainty = TRUE, btarg = 0.48, minbthresh = 0.20)

# Reset working directory to mse base:
set_mse_wd()

# Run base operating model (OM):
setwd(file.path(getwd(), om_test_dir))
system(paste(ss3_bin, '-nohess -maxfn 1'))

# Get SS outputs for OM into R format, then produce and save plots to file:
test_om_output <- r4ss::SS_output(dir = getwd(), covar = FALSE)
SS_plots(replist = test_om_output, uncertainty = FALSE, btarg = 0.48, minbthresh = 0.20)

# Reset working directory to mse base:
set_mse_wd()

# Get data for plot of AM vs EM:
test_am_sp_bio <- tibble(Year = test_am_output$timeseries$Yr,
                         SpawnBio = 2 * test_am_output$timeseries$SpawnBio, 
                         Model = "AM")
  
test_om_sp_bio <- tibble(Year = test_om_output$timeseries$Yr,
                        SpawnBio = test_om_output$timeseries$SpawnBio, 
                        Model = "OM")

test_base_sp_bio <- rbind(test_am_sp_bio, test_om_sp_bio) %>%
  filter(Year %in% base_start_year:base_end_year)

# Create plot of AM vs EM:
base_spawn_bio_plot_facet <- ggplot(test_base_sp_bio, aes(x = Year, y = SpawnBio, colour = Model)) +
  geom_line(size = 1.1) + 
  labs(x = 'Year', y = 'Spawning Biomass (tonnes)') +
  facet_grid(Model ~ ., scales = "free_y") +
  theme(axis.title = element_text(size = 14)) +
  theme(axis.text = element_text(size = 12)) +
  theme(strip.text.y = element_text(size=12)) +
  scale_color_grey(start = 0.3, end = 0.6)

# Create plot of AM vs EM (facet):
base_spawn_bio_plot <- ggplot(test_base_sp_bio, aes(x = Year, y = SpawnBio, colour = Model)) +
  geom_line(size = 1.1) + 
  labs(x = 'Year', y = 'Spawning Biomass (tonnes)') +
  #facet_grid(Model ~ ., scales = "free_y") +
  theme(axis.title = element_text(size = 14)) +
  theme(axis.text = element_text(size = 12)) +
  theme(strip.text.y = element_text(size=12)) +
  scale_color_grey(start = 0.3, end = 0.6)

# Print plot to screen and save to file if required:
print(base_spawn_bio_plot)
print(base_spawn_bio_plot_facet)

if(plot_save == TRUE) ggsave(filename = paste0("am-v-om_spawning_biomass_facet_bw.png"), 
                             path = test_date_dir, plot = base_spawn_bio_plot_facet, device = "png")

if(plot_save == TRUE) ggsave(filename = paste0("am-v-om_spawning_biomass_compare_bw.png"), 
                             path = test_date_dir, plot = base_spawn_bio_plot, device = "png")

#------------------------
# EM SIMULATION SECTION
# -----------------------

# Run a series of n EM sampling/estimation sims:
for(sim in 1:n_sims){
  
  # Reset working directory to mse base:
  set_mse_wd()

  # Set a test seed:
  sim_seed <-  123 + sim
  
  # Create new directory for this simulation test:
  em_test_dir_sim <- file.path(em_test_dir, sim)
  dir.create(em_test_dir_sim)
  
  # Get file names from base OM:
  em_base_files <- list.files(em_base_dir, full.names = TRUE)
   
  # Copy EM files to required simulation directory:
  file.copy(em_base_files, em_test_dir_sim, recursive=TRUE)
  
  # Sample data from OM for base estimation model (EM):
  base_om_dat <- SS_readdat(file.path(om_test_dir, om_dat_new), section = 2, verbose=FALSE)
  base_em_dat <- file.path(em_test_dir_sim, em_dat)
  
  sample_agecomp(dat_list = base_om_dat, seed = sim_seed, cpar = NA,
                 outfile = base_em_dat, Nsamp = list(age_samp_n, age_samp_n), 
                 years = list(seq(base_start_year, base_end_year, by = age_samp_freq), seq(base_start_year, base_end_year, by = age_samp_freq)))
  
  
  base_om_dat <- SS_readdat(base_em_dat, verbose = FALSE)
  
  sample_lcomp(dat_list = base_om_dat, seed = sim_seed, cpar = NA,
               outfile = base_em_dat, Nsamp = list(len_samp_n, len_samp_n),
               years = list(seq(base_start_year, base_end_year, by = len_samp_freq), seq(base_start_year, base_end_year, by = len_samp_freq)))
  
  base_om_dat <- SS_readdat(base_em_dat, verbose = FALSE)
  
  sample_index(dat_list = base_om_dat, fleets = c(2,3), seed = sim_seed,
               outfile = base_em_dat, sds_obs = list(surv_samp_err, surv_samp_err),
               years = list(seq(base_start_year, base_end_year, by = surv_samp_freq), seq(base_start_year, base_end_year, by = surv_samp_freq)))
  
  
  # Now set WD for base estimation model (EM):
  setwd(file.path(getwd(), em_test_dir_sim))
  system(paste(ss3_bin, '-nohess'))
  
}

# Get SS outputs for first EM into R format, then produce and save plots to file:
test_em_output <- r4ss::SS_output(dir = file.path(em_test_dir, '1'), covar = FALSE)
SS_plots(replist = test_em_output, uncertainty = FALSE, btarg = 0.48, minbthresh = 0.20)

# Get list of simulation directories:
sim_em_dirs <- list.dirs(path = em_test_dir, full.names = TRUE, recursive = TRUE)[-1]

# Get EM outputs into R format, result is a list of lists:
base_em_output <- SSgetoutput(dirvec = sim_em_dirs, getcovar = FALSE, getcomp = FALSE, forecast = FALSE)
  
# Summarise EM results aggregated above into a simplified single list object:
sim_summary_em <- SSsummarize(base_em_output)

# Get data for OM spawning biomass time-series:
spawning_biomass_om <- get_ts(file.path(getwd(), om_test_dir))[, c("Yr", "SpawnBio")]
spawning_biomass_om <- filter(spawning_biomass_om, Yr %in% base_start_year:base_end_year)

# Get data for EM spawning biomass estimates from sim_summary:
spawning_biomass_em <- sim_summary_em$SpawnBio

# Make dataframe for plot of mean simulated biomass plus quantiles:
sp_biomass_plot_data <- spawning_biomass_em %>%
  mutate(Year = Yr) %>%
  select(-c(Label, Yr)) %>%
  mutate(sp_bio_em_median = apply(spawning_biomass_em[1:n_sims], 1, median)) %>%
  mutate(sp_bio_em_75 = apply(spawning_biomass_em[1:n_sims], 1, quantile, probs = 0.75)) %>%
  mutate(sp_bio_em_25 = apply(spawning_biomass_em[1:n_sims], 1, quantile, probs = 0.25)) %>%
  mutate(sp_bio_em_05 = apply(spawning_biomass_em[1:n_sims], 1, quantile, probs = 0.05)) %>%
  mutate(sp_bio_em_95 = apply(spawning_biomass_em[1:n_sims], 1, quantile, probs = 0.95)) %>%
  filter(Year %in% base_start_year:base_end_year) %>%
  mutate(sp_bio_om = spawning_biomass_om$SpawnBio)

# Create plot of mean simulated biomass plus confidence intervals:
sp_biomass_compare_plot <- ggplot(data = sp_biomass_plot_data, aes(x = Year, y = sp_bio_em_median)) + 
  geom_ribbon(aes(ymax = sp_bio_em_95, ymin = sp_bio_em_05), alpha = 0.35) +
  geom_line(linetype = 'dashed') +
  geom_line(aes(y = sp_bio_om), size = 1.1) +
  ylab("Simulated Spawning Biomass (tonnes)") + 
  theme(axis.title = element_text(size = 14)) +
  theme(axis.text = element_text(size = 12))

# Print plot and save if required:
print(sp_biomass_compare_plot)

if(plot_save == TRUE) ggsave(filename = paste0("om-v-em_spawning_biomass_compare.png"), 
                                 path = test_date_dir, plot = sp_biomass_compare_plot, device = "png")

# Get data for OM summary (exploitable) biomass time-series:
summary_biomass_om <- get_ts(file.path(getwd(), om_test_dir))[, c("Yr", "Bio_smry")]
summary_biomass_om <- filter(summary_biomass_om, Yr %in% base_start_year:base_end_year)

# Get data for EM summary biomass estimates from sim_summary:
summary_biomass_em <- sim_summary_em$timeseries$Bio_smry

# Get data for catch data from sim_summary:
summary_biomass_em <- sim_summary_em$timeseries %>%
  select(ModelIndex, Yr, Bio_Smry = "Bio_smry") %>%
  filter(Yr %in% min(summary_biomass_om$Yr) : max(summary_biomass_om$Yr))

# Make dataframe for mean catch timeseries plot:
exp_biomass_plot_data <- summary_biomass_em %>%
  group_by(Yr) %>%
  summarise(Median = median(Bio_Smry),
            Q05 = quantile(Bio_Smry, probs = 0.05),
            Q95 = quantile(Bio_Smry, probs = 0.95)) %>%
  mutate(Exp_Bio_OM = summary_biomass_om$Bio_smry)
            
# Create plot of mean simulated summary biomass plus confidence intervals (EM) vs OM:
exp_biomass_compare_plot <- ggplot(data = exp_biomass_plot_data, aes(x = Yr, y = Median)) + 
  geom_ribbon(aes(ymax = Q95, ymin = Q05), alpha = 0.35) +
  geom_line(linetype = 'dashed') +
  geom_line(aes(y = Exp_Bio_OM), size = 1.1) +
  ylab("Simulated Exploitable Biomass (tonnes)") + 
  theme(axis.title = element_text(size = 14)) +
  theme(axis.text = element_text(size = 12))

# Print plot and save if required:
print(exp_biomass_compare_plot)

if(plot_save == TRUE) ggsave(filename = paste0("om-v-em_exploitable_biomass_compare.png"), 
                             path = test_date_dir, plot = exp_biomass_compare_plot, device = "png")


# Get relative error between base models (OM vs median-EM)
ssb_base_re <- (sp_biomass_plot_data$sp_bio_em_median - spawning_biomass_om$SpawnBio) / spawning_biomass_om$SpawnBio

# Plot relative error
plot(y = ssb_base_re, x = base_start_year:base_end_year, 
     ylab = "Relative Error: OM vs EM (Median)",
     xlab = "Year",
     ylim = c(-0.25, 0.25))
abline(h = 0.0, lty = 2)

# Extra diagnostics on estimated recruitment:
# -------------------------------------------

# Show estimation model recruitment outputs:
show(base_em_output$recruit)

# Get predicted recruitment for base model years:
base_em_pred_recr <- base_em_output$recruit[1:which(base_em_output$recruit$year == base_end_year), c("year", "pred_recr")]

# Get mean value of predicted recruitment:
base_em_pred_recr_mean <- mean(base_em_pred_recr$pred_recr)

# Plot predicted recruitment and its mean:
base_em_pred_recr_plot <- ggplot(base_em_pred_recr, aes(x = year, y = pred_recr)) + 
                          geom_line() +
                          labs(x = "Year", y = "Predicted Recruitment Numbers") +
                          geom_hline(yintercept = base_em_pred_recr_mean, colour = "blue")
                          

# End of file -----------------------------------------